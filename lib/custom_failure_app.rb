class CustomFailureApp < Devise::FailureApp
  def respond
    if warden_options[:recall]
      env["PATH_INFO"]  = attempted_path
      self.response = recall_app(warden_options[:recall]).call(env)
    elsif request.format == :json
      json_failure
    else
      super
    end
  end

  def json_failure
    self.status = 401
    self.content_type = request.format.to_s
    self.response_body = '{"error": "Authentication error", "code": "invalid_login"}'
  end
end
