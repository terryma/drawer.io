require "exceptions"

namespace :db do
  desc "Backfill the popularity counter on items."
  task backfill_popularity: :environment do
    Item.find_each do |item|
      item.update_attributes({popularity: item.user_items.count})
    end
  end
end
