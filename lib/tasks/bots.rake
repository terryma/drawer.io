namespace :db do
  desc "Create bot accounts"
  task create_bot_accounts: :environment do
    User.create(email: "bot@hackernews.com",
                password: "hackernews",
                password_confirmation: "hackernews")
  end
end
