require "exceptions"

namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    User.create(email: "zhenchuan.ma@gmail.com",
                 password: "foobar",
                 password_confirmation: "foobar")

    # 20.times do |n|
      # User.create!(email: Faker::Internet.email,
                   # password: "foobar",
                   # password_confirmation: "foobar")
    # end

    # user = User.find_by_email("zhenchuan.ma@gmail.com")
    # 50.times do
      # url = Faker::Internet.url
      # SaveEvent.new_save_event(user, { url: url, client: "Faker" })
    # end

    # urls = File.new(File.expand_path("../urls.txt", __FILE__)).readlines
    # urls[0..200].each do |url|
      # UserItem.new_item(user, { url: url, client: "Sample" }, skip_screenshot: true, skip_scraping:true, error_on_exists: false)
    # end
  end

  task add_fake_users: :environment do
    10.times do |n|
      email = Faker::Internet.email
      User.create!(email: email,
                   password: "password",
                   password_confirmation: "password")
      puts "Added user with email #{email}"
    end
  end
end
