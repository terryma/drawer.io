module Exceptions
  class ClientError < StandardError; end
  class ServerError < StandardError; end

  class URLAlreadyExists < ClientError
    attr_reader :user_item
    def initialize(user_item)
      @user_item = user_item
    end

    def message
      "URL already exists."
    end

    def code
      "url_already_exists"
    end
  end

  class URLInvalid < ClientError
    def message
      "URL is invalid."
    end

    def code
      "url_invalid"
    end
  end

  class URLUnresolvable < ClientError
    def message
      "Could not resolve URL."
    end

    def code
      "url_unresolvable"
    end
  end

end

