// Stores the id for the item just saved
var id;

// Whether we've already focused on the input text field
var focused = false;

// Whether the transition is complete
var transitionDone = false;

// Auto hide the menu bar if no action is detected for 2 seconds
var autoHide = true;

// Respond function to the initial request from the background page. When we
// respond to this, the background page will go ahead and hide this iframe
var respondFunc;

safari.self.addEventListener("message", performCommand, false);

function performCommand(event) {
  if (event.name == "done") {
    var message;
    var clazz;
    var showSummary = false;
    var summary;

    if (event.message.success) {
      message = "Saved";
      clazz = "success";
      showSummary = true;
      id = event.message.id;
    } else {
      var errorCode = event.message.error_code;
      if (errorCode == 'url_already_exists') {
        message = "You already saved this link.";
        showSummary = true;
        id = event.message.id;
        summary = event.message.summary;
      } else if (errorCode == 'invalid_login') {
        message = "Please <a target='_blank' href='http://staging.drawer.io'>log in</a> first.";
      }
      clazz = "error";
    }
    $('#message').html(message).removeClass('loading').addClass(clazz);
    if (showSummary) {
      $('#message').removeClass('no-input');
      var e = $('#summary').show().find(':text').val(summary);
      if (!focused && transitionDone) {
        e.focus();
      }
    }

    setTimeout(function() {
      if (autoHide) {
        closeBar();
      }
    }, 2000);
  }
}

function closeBar() {
  safari.self.tab.dispatchMessage("close", "close");
}

function updateSummary(id, summary) {
  safari.self.tab.dispatchMessage("update_summary", {id: id, summary: summary});
}

$(document).ready(function() {
  // Only focus after the CSS transition is complete to prevent against flicker
  setTimeout(function() {
    if ($('#summary').is(":visible")) {
      $('#summary').find(':text').focus();
      focused = true;
    }
    transitionDone = true;
  }, 500);

  $('#summary').submit(function() {
    var summary = $(':text').val();
    updateSummary(id, summary);
    return false;
  });

  // Detect changes with input
  $('#summary :text').keyup(function(e) {
    // Cancel the hide
    autoHide = false;

    if (e.keyCode == 27) { // escape
      closeBar();
    }
  });
});
