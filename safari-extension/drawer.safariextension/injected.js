if (window.top === window) {
  // The parent frame is the top-level frame, not an iframe.

  var iframeId = 'drawer-io-iframe';

  $(document).keydown(function(e) {
    // Ctrl-D
    if (event.which == 68 && event.ctrlKey) {
      safari.self.tab.dispatchMessage("open", "open");
    }
  });

  safari.self.addEventListener("message", performCommand, false);

  function performCommand(event) {
    if (event.name == "get_auth_token") {
      getAuthToken();
    } else if (event.name == "save") {
      injectIframe();
    } else if (event.name == "close") {
      iframe = document.getElementById(iframeId);
      iframe.style.top = '-100px';
    }
  }

  function getAuthToken() {
    var iframe = document.createElement("iframe");
    iframe.src = safari.extension.baseURI + "get_auth_token.html";
    iframe.style.width = '1px';
    iframe.style.height = '1px';
    document.body.appendChild(iframe);
  }

  function injectIframe() {
    var iframe = document.getElementById(iframeId);
    if (iframe) {
      iframe.parentNode.removeChild(iframe);
    }
    iframe  = document.createElement("iframe");
    iframe.src  = safari.extension.baseURI + "main.html";
    iframe.id = iframeId;
    iframe.style.width = '100%';
    iframe.style.position = 'fixed';
    iframe.style.top = '-100px';
    iframe.style.left = '0px';
    iframe.style.height = '100px';
    iframe.style.margin = '0';
    iframe.style.padding = '0';
    iframe.style.border = '0';
    iframe.style.zIndex = '99999';
    iframe.style.WebkitTransition = 'all 0.5s ease-in-out';
    document.body.appendChild(iframe);

    // Needs to wait a little before the element is ready before the CSS transition
    // can take place
    window.setTimeout(function() {
      iframe.style.top = '0px';
    }, 100);
  }

}
