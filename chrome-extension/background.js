// TODO Implement the login flow instead of letting the user login at the site
// TODO Update the icon. Also swap the icon when the link has already been saved

var DOMAIN = "http://staging.drawer.io";
// var DOMAIN = "http://localhost:3000";
var AUTH_TOKEN_COOKIE = "auth_token";


function sendMessage(id, message) {
  chrome.tabs.sendMessage(id, message, function(response) {
    // When we get a response back, go ahead and tell the parent to hide the
    // child iframe
    chrome.tabs.sendMessage(id, {recipient: 'parent', message: 'hide'});
  });
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.action == 'update_summary') {
    // Read the auth token from the cookie
    chrome.cookies.get({"url": DOMAIN, "name": AUTH_TOKEN_COOKIE}, function(cookie) {
      // We're always going to assume that cookie exists, since the likelihood
      // of it disappearing between the initial save and now is next to none
      var auth_token = unescape(cookie.value);
      var params = {};
      params.user_summary = request.summary;
      $.ajax({
        url: DOMAIN+'/api/v1/items/'+request.id,
        data: params,
        dataType: "json",
        type: 'put',
        headers: { Authorization: 'Token token=' + auth_token },
      }).done(function(data) {
        sendResponse({success: true});
      }).fail(function(jqXHR) {
        var status = jqXHR.status;
        var response = $.parseJSON(jqXHR.responseText);
        var errorCode = response.code;
        sendResponse({success: false, error_code: errorCode});
      });
    });
  }
  return true;
});

chrome.browserAction.onClicked.addListener(function(tab) {
  // Load the iframe
  chrome.tabs.executeScript(null, {file:"iframeInjector.js"}, function() {
    chrome.tabs.query({ currentWindow: true, active: true }, function(tabs) {
      params = {};
      params.url = tabs[0].url;
      params.client = "Chrome";

      // Read the auth token from the cookie
      chrome.cookies.get({"url": DOMAIN, "name": AUTH_TOKEN_COOKIE}, function(cookie) {
        // Cookie doesn't exist. This could happen for new users or people
        // who've cleared their cookies
        if (!cookie) {
          sendMessage(tabs[0].id, {recipient: 'iframe', success: false, errorCode: "invalid_login"});
        } else {
          var auth_token = unescape(cookie.value);
          $.ajax({
            url: DOMAIN+'/api/v1/items',
            data: params,
            dataType: "json",
            type: 'post',
            headers: { Authorization: 'Token token=' + auth_token },
          }).done(function(data) {
            // Tell the content script to update with success
            sendMessage(tabs[0].id, {recipient: 'iframe', success: true, id: data.id});
          }).fail(function(jqXHR) {
            var status = jqXHR.status;
            var response = $.parseJSON(jqXHR.responseText);

            // Tell the content scirpt to update with error
            // id and user_summary are only populated when the error is
            // url_already_exists
            sendMessage(tabs[0].id, {recipient: 'iframe', success: false, id: response.id, summary: response.user_summary, error_code: response.code});
          });
        }
      });
    });
  });
});
