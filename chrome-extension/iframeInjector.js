var iframeId = 'drawer-io-iframe';
var iframe = document.getElementById(iframeId);
if (iframe) {
  iframe.parentNode.removeChild(iframe);
} else {
  // Only add listener once
  chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.recipient == 'parent') {
      if (request.message == 'hide') {
        iframe = document.getElementById(iframeId);
        iframe.style.top = '-100px';
      }
    }
    return true;
  });
}
iframe  = document.createElement("iframe");
iframe.src  = chrome.extension.getURL("main.html");
iframe.id = iframeId;
iframe.style.width = '100%';
iframe.style.position = 'fixed';
iframe.style.top = '-100px';
iframe.style.left = '0px';
iframe.style.height = '100px';
iframe.style.margin = '0';
iframe.style.padding = '0';
iframe.style.border = '0';
iframe.style.zIndex = '99999';
iframe.style.WebkitTransition = 'all 0.5s ease-in-out';
document.body.appendChild(iframe);

// Needs to wait a little before the element is ready before the CSS transition
// can take place
window.setTimeout(function() {
  iframe.style.top = '0px';
}, 100);

