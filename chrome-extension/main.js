// Stores the id for the item just saved
var id;

// Whether we've already focused on the input text field
var focused = false;

// Whether the transition is complete
var transitionDone = false;

// Auto hide the menu bar if no action is detected for 2 seconds
var autoHide = true;

// Respond function to the initial request from the background page. When we
// respond to this, the background page will go ahead and hide this iframe
var respondFunc;

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.recipient == 'iframe') {
    var message;
    var clazz;
    var showSummary = false;
    var summary;

    respondFunc = sendResponse;
    if (request.success) {
      message = "Saved";
      clazz = 'success';
      showSummary = true;
      id = request.id;
    } else {
      errorCode = request.error_code;
      if (errorCode == 'url_already_exists') {
        message = "You already saved this link.";
        showSummary = true;
        id = request.id;
        summary = request.summary;
      } else if (errorCode == 'invalid_login') {
        message = "Please <a target='_blank' href='http://staging.drawer.io'>log in</a> first.";
      }
      clazz = 'error';
    }
    $('#message').html(message).removeClass('loading').addClass(clazz);
    if (showSummary) {
      $('#message').removeClass('no-input');
      var e = $('#summary').show().find(':text').val(summary);
      if (!focused && transitionDone) {
        e.focus();
      }
    }
    setTimeout(function() {
      if (autoHide) {
        respondFunc();
      }
    }, 2000); // Stay on screen for 2 seconds, since css transition takes 1
  }
  return true;
})

function updateSummary(id, summary) {
  chrome.runtime.sendMessage({action: 'update_summary', id: id, summary: summary}, function(response) {
    respondFunc();
  });
}

$(document).ready(function() {
  // Only focus after the CSS transition is complete to prevent against flicker
  setTimeout(function() {
    if ($('#summary').is(":visible")) {
      $('#summary').find(':text').focus();
      focused = true;
    }
    transitionDone = true;
  }, 500);

  $('#summary').submit(function() {
    var summary = $(':text').val();
    updateSummary(id, summary);
    return false;
  });

  // Detect changes with input
  $('#summary :text').keyup(function(e) {
    // Cancel the hide
    autoHide = false;

    if (e.keyCode == 27) { // escape
      respondFunc();
    }
  });
});
