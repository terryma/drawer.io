require 'spec_helper'

describe "User pages" do

  subject { page }

  describe "signup" do

    before { visit root_path }

    let(:submit) { "Create Free Account" }

    describe "with invalid information" do
      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end
    end

    describe "with valid information" do
      before do
        within('#new_user') do
          fill_in "Email",        with: "user@example.com"
          fill_in "Password",     with: "foobar"
        end
      end

      it "should create a user" do
        expect { click_button submit }.to change(User, :count).by(1)
      end
    end
  end

  describe "signin" do
    before { visit root_path }

    describe "with invalid information" do
      before { click_link "Sign in" }

      it { should have_title('Sign in') }
      it { should have_selector('div.alert.alert-error', text: 'Invalid') }
    end

    describe "with valid information" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        fill_in "Email",    with: user.email.upcase
        fill_in "Password", with: user.password
        click_button "Sign in"
      end

      it { should have_title(user.name) }
      it { should have_link('Profile',     href: user_path(user)) }
      it { should have_link('Sign out',    href: signout_path) }
      it { should_not have_link('Sign in', href: signin_path) }
    end
  end

  describe "profile page" do
    let(:user) { FactoryGirl.create(:user) }
    let!(:s1) { FactoryGirl.create(:save_event, user: user, content: "Foo") }
    let!(:s2) { FactoryGirl.create(:save_event, user: user, content: "Bar") }

    before { visit user_path(user) }

    it { should have_content(user.name) }
    it { should have_title(user.name) }

    describe "save events" do
      it { should have_content(s1.url) }
      it { should have_content(s2.url) }
      it { should have_content(user.save_events.count) }
    end
  end

end
