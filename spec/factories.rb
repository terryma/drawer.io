FactoryGirl.define do
  factory :user do
    name "Terry Ma"
    email "hi@terry.ma"
    password "foobar"
    password_confirmation "foobar"
  end

  factory :save_event do
    url "http://www.google.com"
    client "Chrome"
    user
  end
end

