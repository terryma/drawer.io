# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130917122743) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"

  create_table "events", force: true do |t|
    t.string   "type"
    t.string   "client"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "related_user_id"
    t.integer  "related_item_id"
  end

  create_table "item_snapshots", force: true do |t|
    t.integer  "item_id"
    t.date     "date"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "screenshot_file_name"
    t.string   "screenshot_content_type"
    t.integer  "screenshot_file_size"
    t.datetime "screenshot_updated_at"
    t.text     "title"
    t.text     "description"
  end

  add_index "item_snapshots", ["item_id", "date"], name: "index_item_snapshots_on_item_id_and_date", using: :btree

  create_table "items", force: true do |t|
    t.string   "canonical_url"
    t.string   "domain"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uuid"
    t.integer  "popularity",    default: 0
  end

  add_index "items", ["canonical_url"], name: "index_items_on_canonical_url", unique: true, using: :btree
  add_index "items", ["uuid"], name: "index_items_on_uuid", unique: true, using: :btree

  create_table "relationships", force: true do |t|
    t.integer  "fan_id"
    t.integer  "idol_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "relationships", ["fan_id", "idol_id"], name: "index_relationships_on_fan_id_and_idol_id", unique: true, using: :btree
  add_index "relationships", ["fan_id"], name: "index_relationships_on_fan_id", using: :btree
  add_index "relationships", ["idol_id"], name: "index_relationships_on_idol_id", using: :btree

  create_table "user_items", force: true do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.string   "user_summary"
    t.datetime "last_save_timestamp"
    t.integer  "last_snapshot_id"
    t.string   "last_url"
    t.boolean  "deleted",             default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.text     "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "authentication_token"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
