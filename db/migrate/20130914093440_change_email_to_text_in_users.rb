class ChangeEmailToTextInUsers < ActiveRecord::Migration
  def change
    change_column :users, :email, :text
  end
end
