class AddRelatedItemToEvent < ActiveRecord::Migration
  def change
    add_column :events, :related_item_id, :integer
  end
end
