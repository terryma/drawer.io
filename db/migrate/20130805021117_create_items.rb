class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :canonical_url
      t.string :domain

      t.timestamps
    end
  end
end
