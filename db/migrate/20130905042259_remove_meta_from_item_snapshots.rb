class RemoveMetaFromItemSnapshots < ActiveRecord::Migration
  def change
    remove_column :item_snapshots, :meta, :text
  end
end
