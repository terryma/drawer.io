class AddTitleDescriptionToItemSnapshots < ActiveRecord::Migration
  def change
    add_column :item_snapshots, :title, :string
    add_column :item_snapshots, :description, :string
  end
end
