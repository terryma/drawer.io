class CreateUserItems < ActiveRecord::Migration
  def change
    create_table :user_items do |t|
      t.integer :user_id
      t.integer :item_id
      t.string :user_summary
      t.timestamp :last_save_timestamp
      t.integer :last_snapshot_id
      t.string :last_url
      t.boolean :deleted

      t.timestamps
    end
  end
end
