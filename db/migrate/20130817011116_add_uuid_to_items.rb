class AddUuidToItems < ActiveRecord::Migration
  def change
    add_column :items, :uuid, :string
    add_index :items, :uuid, :unique => true
  end
end
