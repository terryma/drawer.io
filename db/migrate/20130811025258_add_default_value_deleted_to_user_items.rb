class AddDefaultValueDeletedToUserItems < ActiveRecord::Migration
  def change
    change_column_default :user_items, :deleted, false
  end
end
