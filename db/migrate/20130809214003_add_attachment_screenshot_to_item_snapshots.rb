class AddAttachmentScreenshotToItemSnapshots < ActiveRecord::Migration
  def self.up
    change_table :item_snapshots do |t|
      t.attachment :screenshot
    end
  end

  def self.down
    drop_attached_file :item_snapshots, :screenshot
  end
end
