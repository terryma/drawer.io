class AddIndexToItems < ActiveRecord::Migration
  def change
    add_index :items, :canonical_url, unique: true
  end
end
