class AddMetaToItemSnapshots < ActiveRecord::Migration
  def change
    add_column :item_snapshots, :meta, :text
  end
end
