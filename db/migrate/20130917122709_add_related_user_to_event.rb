class AddRelatedUserToEvent < ActiveRecord::Migration
  def change
    add_column :events, :related_user_id, :integer
  end
end
