class AddDefaultPopularityZeroToItems < ActiveRecord::Migration
  def change
    change_column_default :items, :popularity, 0
  end
end
