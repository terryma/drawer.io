class CreateItemSnapshots < ActiveRecord::Migration
  def change
    create_table :item_snapshots do |t|
      t.integer :item_id
      t.date :date
      t.text :content

      t.timestamps
    end
    add_index :item_snapshots, [:item_id, :date]
  end
end
