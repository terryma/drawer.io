class ChangeTitleDescriptionToTextInItemSnapshots < ActiveRecord::Migration
  def change
    change_column :item_snapshots, :title, :text
    change_column :item_snapshots, :description, :text
  end
end
