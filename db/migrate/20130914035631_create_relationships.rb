class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :fan_id
      t.integer :idol_id

      t.timestamps
    end

    add_index :relationships, :fan_id
    add_index :relationships, :idol_id
    add_index :relationships, [:fan_id, :idol_id], unique: true
  end
end
