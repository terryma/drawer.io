# Responsible for parsing popular contents on the web and insert them into bot
# accounts

class BotWorker
  include Sidekiq::Worker

  ENV_TO_HOST = {
    development: 'localhost:3000',
    staging: 'staging.drawer.io',
    production: 'drawer.io'
  }.with_indifferent_access

  EMAIL = "bot@hackernews.com"

  def initialize()
    @@host ||= ENV_TO_HOST[ENV['RAILS_ENV']]
    @@auth_token ||= User.find_by_email(EMAIL).authentication_token
  end

  # Pick a random link and add it
  def perform()
    entry = RubyHackernews::Entry.all.sample
    url = entry.link.href
    begin
      RestClient.post "http://#{@@host}/api/v1/items",
      { url: url, client: 'Bot' }.to_json,
        content_type: :json,
        accept: :json,
        Authorization: %Q{Token token="#{@@auth_token}"}
    rescue => e
      logger.info "Post failed with #{e.response.to_str}"
    end
  end

end
