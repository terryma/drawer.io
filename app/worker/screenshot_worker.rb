class ScreenshotWorker
  include Sidekiq::Worker

  def perform(id)
    logger.info "Generating screenshot for snapshot id #{id}"
    snapshot = ItemSnapshot.find(id)
    logger.info "Found snapshot with id #{id}"
    screenshot = get_screenshot(snapshot.item.canonical_url)
    logger.info "Grabbed screenshot for url #{snapshot.item.canonical_url}"
    snapshot.update_attributes!({screenshot: screenshot})
    logger.info "Saved screenshot to S3"
    screenshot.unlink
    logger.info "Queue screenshot generation message to redis"
    queue_new_screenshot(snapshot)
  end

  private

    # Grab a png screenshot using IMGKit
    def get_screenshot(url)
      img = IMGKit.new(url,
                       width: 300,
                       height: 150,
                       "crop-w" => 300,
                       "crop-h" => 150,
                       "zoom" => 0.25,
                       "disable-smart-width" => true,
                       "load-error-handling" => 'ignore').to_png
      file = Tempfile.new(["screenshot", '.png'], 'tmp', encoding: 'ascii-8bit')
      file.write(img)
      file.flush
      file
    end

    def queue_new_screenshot(snapshot)
      # FIXME Hacky, move this into some global space
      redis = get_redis()
      logger.info "Pushing snapshot id #{snapshot.id} to redis..."
      redis.lpush("screenshot_#{snapshot.id}", {url: snapshot.screenshot.url}.to_json)
      logger.info "Finished queueing message to redis"
    end

    def get_redis()
      if %w(staging production).include?(ENV['RAILS_ENV'])
        uri = URI.parse(ENV['REDISCLOUD_URL'])
        redis = Redis.new(host: uri.host, port: uri.port, password: uri.password)
      else
        redis = Redis.new
      end
    end
end
