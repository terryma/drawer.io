# Responsbile for scraping the page content and popualate title, description,
# and 'reader' view content for a snapshot
class SnapshotWorker
  include Sidekiq::Worker

  def perform(id)
    logger.info "Scraping content for snapshot id #{id}"
    snapshot = ItemSnapshot.find(id)
    logger.info "Found snapshot with id #{id}"
    meta = get_meta(snapshot.item.canonical_url)
    snapshot.update_attributes!({title: meta[:title], description: meta[:description], content: meta[:content]})
    logger.info "Updated snapshot info"
  end

  private
    # TODO Refactor with user_item.rb
    def get_meta(url)
      page = Pismo::Document.new(url, allow_redirections: :all)
      description = page.description.blank? ? page.lede : page.description
      { title: page.title, description: description, content: page.html_body }
    end
end
