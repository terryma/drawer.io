App.controller 'RegistrationsController', ['$scope', '$parse', '$location', '$route', 'Session', ($scope, $parse, $location, $route, Session) ->

  $scope.email = ''
  $scope.password = ''

  # TODO: Add client side angularjs validation for a better user experience
  $scope.save = (e) ->
    e.preventDefault()
    Session.register(
      $scope.email,
      $scope.password,
      (data, status, headers, config) ->
        Session.signIn(data.auth_token, data.email)
        $route.reload()
      ,
      (data, status, headers, config) ->
        errors = data.errors
        for field, msg of errors
          console.log(field + ', ' + msg)
          message = $parse('form.'+field+'.$error.server_message')
          message.assign($scope, msg[0])
    )
]
