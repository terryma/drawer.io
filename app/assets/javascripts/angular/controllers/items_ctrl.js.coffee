App.controller 'ItemController', ['$scope', '$q', 'Item', ($scope, $q, Item) ->

  # How many items to load per pagination call
  ITEMS_PER_PAGE = 10

  # Store all the links on the page currently
  $scope.items = []

  # Tracks pagination. Used by infinite scroll.
  $scope.currentPage = 1

  # Tracks whether or not we should load more items. Used by infinite scroll.
  $scope.done = false

  # Controls whether the ajax loading spinner is shown or not
  $scope.loading = false

  # Current item we're dealing with when a new link needs to be created
  $scope.item = null

  # This is used to make sure only a single(the last) query is considered
  # current. All the stale deferred queries will be rejected
  $scope.deferredQuery = null

  # Flag to indicate whether there's already a pagination request in flight so
  # we don't make more unncessary requests.
  $scope.paginating = false

  # Used by infinite scroll to load more pages.
  $scope.more = ->
    # If there's already a pagination request in flight, don't do anything
    return if $scope.paginating
    $scope.paginating = true

    deferred = $q.defer()
    promise = deferred.promise
    load(deferred)

    promise.then (items) ->
      $scope.items = $scope.items.concat(items)
      $scope.paginating = false

  # Save a new item
  $scope.save = ->
    $scope.loading = true
    item = new Item($scope.item)
    item.url = 'http://'+item.url
    item.client = 'Web'
    item.$save(
      (data) ->
        new_item = new Item(data)
        if new_item.has_screenshot
          # If the item already has a screenshot, simply display it
        else
          # Otherwise, we fire a request to the backend asking for the
          # screenshot, using the snapshot id as a key
          load_screenshot(new_item)
        $scope.items.unshift(new_item)
        $scope.loading = false
      handle_error
    )

  # Delete an item. TODO Handle errors
  $scope.delete = (item) ->
    index = $scope.items.indexOf(item)
    item.$delete()
    $scope.items.splice(index, 1)

  # Update an item.
  $scope.update = (item) ->
    item.refreshing = true
    index = $scope.items.indexOf(item)
    Item.update({id:item.id},
      (data) ->
        item = new Item(data)
        $scope.items[index] = item
        load_screenshot(item)
      handle_error
    )

  # Display the scrubbed page content in a modal
  $scope.read = (item) ->
    $('#item-content .item-content-title').text(item.title)
    $('#item-content .item-content-body').html(item.content)
    $('#item-content').foundation('reveal', 'open')

  # If the query changes, reload all the items starting from page 1.
  $scope.$watch 'query', (newQuery, oldQuery) ->
    # Don't do anything if nothing changed (initialization)
    if (newQuery == oldQuery)
      return

    $scope.done = false
    $scope.currentPage = 1
    $scope.items = []

    rejection_reason = "stale"
    # If a promise already exists, reject it
    if $scope.deferredQuery
      $scope.deferredQuery.reject(rejection_reason)
    $scope.deferredQuery = $q.defer()
    load($scope.deferredQuery)
    $scope.searching = true
    $scope.deferredQuery.promise.then(
      (items) ->
        $scope.items = items
        $scope.searching = false
      (reason) ->
        if (reason == rejection_reason)
          # We simply ignore the stale data
          return
    )

  handle_error = (response) ->
    if response.data.error
      $scope.error = response.data.error
    else
      $scope.error = 'Houston, we have a problem.  (╯°□°）╯︵ ┻━┻)'
    $scope.loading = false

  handleCallback = (event) ->
    event.target.close()
    url = JSON.parse(event.data).url
    $scope.$apply( () ->
      for item in $scope.items
        if 'screenshot_'+item.snapshot_id == event.type
          item.screenshot = url
          # We're no longer loading the screenshot
          item.loading_screenshot = false
          break
    )

  load_screenshot = (item) ->
    snapshot_id = item.snapshot_id
    source = new EventSource('/sse?snapshot_id='+snapshot_id)
    source.addEventListener('screenshot_'+snapshot_id, handleCallback, false)
    # Set the loading screenshot flag to true so we can display a little
    # spinner
    item.loading_screenshot = true

  # Load more items. Used by both infinite scrolling and query
  load = (deferred) ->
    params = {
      page: $scope.currentPage,
      per: ITEMS_PER_PAGE
    }
    angular.extend(params, {q: $scope.query}) if $scope.query

    Item.query(
      params
      (data) ->
        deferred.resolve(data)
        # $scope.items.push item for item in data
        if (data.length < ITEMS_PER_PAGE)
          $scope.done = true
        else
          # things could get out of order here. the request for page 2 could
          # come back earlier than page 1. if this is already set to true, don't
          # set it back to false again
          $scope.done = false if !$scope.done
    )
    $scope.currentPage += 1
]
