App.controller 'HomeController', ['$scope', '$location', '$route', '$routeParams', '$cookieStore', 'Session', 'Events', 'user', '$rootScope', ($scope, $location, $route, $routeParams, $cookieStore, Session, Events, user, $rootScope) ->

  # This assignment won't take place until 'user' is ready. This way we have the
  # user's state ready in the view to render things correctly
  $scope.user = user
  $scope.signedIn = $scope.user.authenticated
  $scope.gravatar = $scope.user.gravatar
  Events.load().then (response) ->
    $scope.events = response.data

  # Store the login states in an object. Since this model is currently in a
  # ng-include, an object is required
  $scope.login = {}

  $scope.login = ->
    Session.login(
      $scope.login.email,
      $scope.login.password,
      (data, status, headers, config) ->
        ###
        {
          "success": true,
          "info": "Logged in",
          "user": {
            "id": 7,
            "email": "zhenchuan.ma@gmail.com",
            "created_at": "2013-08-21T21:30:09.889Z",
            "updated_at": "2013-08-25T17:38:30.351Z"
          },
          "auth_token": "2xq7szHDMJvXWqcZfpav"
        }
        ###
        Session.signIn(data.auth_token, data.user.email)
        $('#sign-in-modal').foundation('reveal', 'close')
        $route.reload()
      ,
      (data, status, headers, config) ->
        $scope.login.error = data.error
    )

  # When logout is clicked. We log the user out and redirect back to the
  # homepage
  $scope.$on '$routeChangeSuccess', (event, $currentRoute, $previousRoute) ->
    path = $location.path()
    if path == '/logout'
      Session.logout(
        (data, status, headers, config) ->
          $location.path('/')
        ,
        (data, status, headers, config) ->
          console.log('log out failed...')
          $location.path('/')
      )

]
