# Handles /feed page
App.controller 'FeedController', ['$scope', '$http', '$q', 'Events', 'user', ($scope, $http, $q, Events, user) ->
  # This assignment won't take place until 'user' is ready. This way we have the
  # user's state ready in the view to render things correctly
  $scope.user = user
  $scope.gravatar = $scope.user.gravatar
  $scope.signedIn = $scope.user.authenticated
  Events.load().then (response) ->
    $scope.events = response.data

  # TODO Turn into service
  do ->
    $http.get('/api/v1/idols')
      .success (data) ->
        $scope.idols = data
      .error ->
        console.log('uh oh')


  # How many items to load per pagination call
  ITEMS_PER_PAGE = 10

  # Store all the items on the page currently
  $scope.items = []

  # Tracks pagination. Used by infinite scroll.
  $scope.currentPage = 1

  # Tracks whether or not we should load more items. Used by infinite scroll.
  $scope.done = false

  # Controls whether the ajax loading spinner is shown or not
  $scope.loading = false

  # This is used to make sure only a single(the last) query is considered
  # current. All the stale deferred queries will be rejected
  $scope.deferredQuery = null

  # Flag to indicate whether there's already a pagination request in flight so
  # we don't make more unncessary requests.
  $scope.paginating = false

  # Used by infinite scroll to load more pages.
  $scope.more = ->
    # If there's already a pagination request in flight, don't do anything
    return if $scope.paginating
    $scope.paginating = true

    deferred = $q.defer()
    promise = deferred.promise
    load(deferred)

    promise.then (items) ->
      $scope.items = $scope.items.concat(items)
      $scope.paginating = false

  # Load more items. Used by both infinite scrolling and query
  load = (deferred) ->
    params = {
      page: $scope.currentPage,
      per: ITEMS_PER_PAGE
    }

    $http.get('/api/v1/feed', {params: params})
      .success (data) ->
        deferred.resolve(data)
        if (data.length < ITEMS_PER_PAGE)
          $scope.done = true
        else
          # things could get out of order here. the request for page 2 could
          # come back earlier than page 1. if this is already set to true, don't
          # set it back to false again
          $scope.done = false if !$scope.done

    $scope.currentPage += 1
]

