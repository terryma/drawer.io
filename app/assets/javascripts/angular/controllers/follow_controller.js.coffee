# Handles /follow page
# TODO A lot of stuff here is copied from ItemController. Refactor plz
App.controller 'FollowController', ['$scope', '$q', '$http', 'User', 'Events', 'user', ($scope, $q, $http, User, Events, user) ->
  # This assignment won't take place until 'user' is ready. This way we have the
  # user's state ready in the view to render things correctly
  $scope.user = user
  $scope.gravatar = $scope.user.gravatar
  $scope.signedIn = $scope.user.authenticated
  Events.load().then (response) ->
    $scope.events = response.data

  $scope.fans = []
  $scope.idols = []

  do ->
    $http.get('/api/v1/idols')
      .success (data) ->
        $scope.idols.push new User(user) for user in data
      .error ->
        console.log('uh oh')


  # How many users to load per pagination call
  USERS_PER_PAGE = 10

  # Store all the users on the page currently
  $scope.users = []

  # Tracks pagination. Used by infinite scroll.
  $scope.currentPage = 1

  # Tracks whether or not we should load more items. Used by infinite scroll.
  $scope.done = false

  # Controls whether the ajax loading spinner is shown or not
  $scope.loading = false

  # This is used to make sure only a single(the last) query is considered
  # current. All the stale deferred queries will be rejected
  $scope.deferredQuery = null

  # Flag to indicate whether there's already a pagination request in flight so
  # we don't make more unncessary requests.
  $scope.paginating = false

  $scope.follow = (user) ->
    $http.post('/api/v1/follow',
      {
        "email": user.email,
      })
      .success ->
        user.is_idol = true
        $scope.idols.push user
      .error ->
        console.log('uh oh')

  $scope.unfollow = (user) ->
    $http.post('/api/v1/unfollow',
      {
        "email": user.email,
      })
      .success ->
        for u, i in $scope.idols when u.email == user.email
          $scope.idols.splice(i, 1)
          break

        for u in $scope.users when u.email == user.email
          u.is_idol = false
          break

      .error ->
        console.log('uh oh')

  # Used by infinite scroll to load more pages.
  $scope.more = ->
    # If there's already a pagination request in flight, don't do anything
    return if $scope.paginating
    $scope.paginating = true

    deferred = $q.defer()
    promise = deferred.promise
    load(deferred)

    promise.then (users) ->
      $scope.users = $scope.users.concat(users)
      $scope.paginating = false

  $scope.$watch 'query', (newQuery, oldQuery) ->
    # Don't do anything if nothing changed (initialization)
    if (newQuery == oldQuery)
      return

    $scope.done = false
    $scope.currentPage = 1
    $scope.users = []

    rejection_reason = "stale"
    # If a promise already exists, reject it
    if $scope.deferredQuery
      $scope.deferredQuery.reject(rejection_reason)
    $scope.deferredQuery = $q.defer()
    load($scope.deferredQuery)
    $scope.searching = true
    $scope.deferredQuery.promise.then(
      (users) ->
        $scope.users = users
        $scope.searching = false
      (reason) ->
        if (reason == rejection_reason)
          # We simply ignore the stale data
          return
    )

  # Load more items. Used by both infinite scrolling and query
  load = (deferred) ->
    params = {
      page: $scope.currentPage,
      per: USERS_PER_PAGE
    }
    angular.extend(params, {q: $scope.query}) if $scope.query

    User.query(
      params
      (data) ->
        deferred.resolve(data)
        if (data.length < USERS_PER_PAGE)
          $scope.done = true
        else
          # things could get out of order here. the request for page 2 could
          # come back earlier than page 1. if this is already set to true, don't
          # set it back to false again
          $scope.done = false if !$scope.done
    )
    $scope.currentPage += 1

]

