App.controller 'ClientsController', ['$scope', 'user', 'Events', ($scope, user, Events) ->
# Handles /clients page
  # This assignment won't take place until 'user' is ready. This way we have the
  # user's state ready in the view to render things correctly
  $scope.user = user
  $scope.signedIn = $scope.user.authenticated
  $scope.gravatar = $scope.user.gravatar
  Events.load().then (response) ->
    $scope.events = response.data

  $scope.browser = BrowserDetect.browser

  $scope.chromeUrl = "https://chrome.google.com/webstore/detail/kfklbagdkpchlinenafojapocgjfmiln"

  $scope.install = ->
    if $scope.browser == BrowserDetect.browser
      chrome.webstore.install(
        $scope.chromeUrl,
        () -> console.log('install success'),
        (msg) -> console.log('install failed', msg)
      )
]

