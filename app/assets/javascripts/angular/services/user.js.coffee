App.factory 'User', ['$resource', ($resource) ->
  return $resource('/api/v1/users/:id', {id: '@id'}, {update: {method: "PUT"}})
]
