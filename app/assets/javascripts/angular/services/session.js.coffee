App.service 'Session', ['$http', '$cookieStore', '$q', '$rootScope', ($http, $cookieStore, $q, $rootScope) ->

  this.login = (email, password, onSuccess, onError) ->
    $http.post('/api/v1/login',
      {
        "user": {
          "email": email,
          "password", password
        }
      })
      .success(onSuccess)
      .error(onError)

  this.logout = (onSuccess, onError) ->
    $http.post('/api/v1/logout').success(onSuccess).error(onError)


  this.signedIn = null
  this.checkSignIn = ->
    # TODO We should cache this request so we don't call it all the time
    ###
    {
      "email": "hi@terry.ma",
      "gravatar": "https://secure.gravatar.com/avatar/4631735d83e5b5889313f60b46fddda6?s=75"
    }
    ###
    return $http.get('/api/v1/profile')
                .then(
                   (data) ->
                     return angular.extend(data.data, {authenticated: true})
                   (data) ->
                     return angular.extend(data.data, {authenticated: false})
                )

  this.signIn = (auth_token, email) ->
    $cookieStore.put('auth_token', auth_token)
    $cookieStore.put('email', email)

  this.register = (email, password, onSuccess, onError) ->
    $http.post('/api/v1/users',
      {
        "user": {
          "email": email,
          "password": password
        }
      })
      .success(onSuccess)
      .error(onError)
]

