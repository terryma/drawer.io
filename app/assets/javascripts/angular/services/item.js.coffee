App.factory 'Item', ['$resource', ($resource) ->
  return $resource('/api/v1/items/:id', {id: '@id'}, {update: {method: "PUT"}})
]
