# Simple directive to insert the chrome-webstore-item link into head
# https://developers.google.com/chrome/web-store/docs/inline_installation
App.directive 'chromeExtension', [() ->
  return {
    restrict: 'A',
    link: (scope, elem, attrs) ->
      $('head').append("<link rel='chrome-webstore-item' href='#{scope.chromeUrl}'>")
  }
]
