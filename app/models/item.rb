class Item < ActiveRecord::Base
  has_many :item_snapshots, -> { where date: Date.today }, dependent: :destroy
  has_many :user_items, -> { where deleted: false }, dependent: :destroy
  validates :canonical_url, presence: true
  validates :domain, presence: true
  before_create :create_uuid

  private

    # Create a UUID for the item. This is used for the file name of screenshot
    # to prevent public access
    def create_uuid
      self.uuid = loop do
        uuid = SecureRandom.urlsafe_base64(nil, false)
        break uuid unless Item.where(uuid: uuid).exists?
      end
    end

end
