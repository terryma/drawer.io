class Relationship < ActiveRecord::Base
  belongs_to :fan, class_name: "User"
  belongs_to :idol, class_name: "User"
  validates :fan_id, presence: true
  validates :idol_id, presence: true

end
