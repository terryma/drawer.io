class Event < ActiveRecord::Base
  belongs_to :related_item, class_name: "Item", foreign_key: "related_item_id"
  belongs_to :related_user, class_name: "User", foreign_key: "related_user_id"
  default_scope -> { order('created_at DESC') }
end

class RegisterEvent < Event
end

class SaveEvent < Event
end

class DeleteEvent < Event
end

class FollowEvent < Event
end

class UnfollowEvent < Event
end

class FollowedEvent < Event
end

class UnfollowedEvent < Event
end
