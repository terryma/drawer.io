class UserItem < ActiveRecord::Base
  belongs_to :user
  belongs_to :item
  accepts_nested_attributes_for :item, update_only: true
  belongs_to :last_snapshot, class_name: "ItemSnapshot"
  default_scope { includes(:last_snapshot).includes(:item).includes(:user).order('last_save_timestamp DESC') }
  before_create { self.last_save_timestamp = Time.now }
  validates :last_snapshot, presence: true
  validates :last_url, presence: true
  validates :user_id, presence: true
  validates :item_id, presence: true

  include PgSearch
  # TODO Add search weight/ranking
  pg_search_scope :search,
    against: [:user_summary, :last_url],
    using: {tsearch: {dictionary: "english", prefix: true}},
    associated_against: {last_snapshot: [:title, :description, :content]},
    order_within_rank: "last_save_timestamp DESC"

  def self.new_item(user, params, options={})
    # The following happens when a save event needs to be persisted
    # 1. Create a new save event, with timestamp, user id, client, url
    # 2. Scrub the url to get the canonical url.
    # 3. Look up item id using canonical url.
    # 4. Look up snapshot id for item for the day
    # 5. If found, set snapshot id on save event to be the snapshot id above
    # 6. If not found, create new snapshot with item id, snapshot date, and
    # content, set snapshot id to be the newly created snapshot
    # 7. Look up user item using item id
    # 8. If found, update last save date, update last snapshot id, update last
    # url
    # 9. If not found, create new UserItem, set item id, set user id, set last
    # save date, set last snapshot id

    defaults = {
      skip_screenshot: false,
      skip_scraping: false,
      error_on_exists: true,
      async_scraping: true
    }
    options = defaults.merge(options)

    url = params[:url]
    client = params[:client]

    # For web client, we scrape the content synchronously. For browser
    # extensions, we do it async to provide a faster saving experience.
    if client == 'Web'
      options[:async_scraping] = false
    end

    # Make sure the url is sound (parsable and reachable) before we proceed
    uri = validate_url(url)

    # TODO Canononize the URL
    # Create an item lazily if one doesn't exist
    item = Item.find_by_canonical_url(url)
    if item.nil?
      item = Item.create!(canonical_url: url, domain: uri.host)
    end

    snapshot = get_snapshot(item, options)

    exists = false
    user_item = user.user_items.find_by_item_id(item)
    if user_item.nil?
      user_item = UserItem.new(item: item, last_url: url, last_snapshot: snapshot, user:user)
      item.update_attributes!(popularity: item.popularity+1)
      user.events.create!(type: 'SaveEvent', client: params[:client], related_item: item)
    elsif user_item.deleted?
      # Restore the deleted item and update the time in which it's considered
      # saved
      user_item.deleted = false
      user_item.last_save_timestamp = Time.now
      item.update_attributes!(popularity: item.popularity+1)
      user.events.create!(type: 'SaveEvent', client: params[:client], related_item: item)
    else
      exists = true
    end

    # Update the snapshot pointer
    if user_item.last_snapshot != snapshot
      user_item.last_snapshot = snapshot
    end

    user_item.save!

    if exists && options[:error_on_exists]
      logger.info "url #{url} already exists for user #{user.email}"
      raise Exceptions::URLAlreadyExists.new(user_item)
    end

    user_item
  end

  # FIXME Update doc
  # Update the snapshot
  # Two possiblities:
  # 1. Snapshot is not from today, get a new snapshot that's for today
  # 2. Snapshot is for today, force a refresh for it
  def self.update_item(user, params)
    # FIXME Make the web ui's update button pass in ?force=true
    logger.info "Update items called with params #{params}"

    user_item = UserItem.find(params[:id])

    # Reload the snapshot if asked
    if (params[:force])
      logger.info "Reloading snapshot on user item #{user_item.id}"
      snapshot = get_snapshot(user_item.last_snapshot.item, skip_screenshot: false, force_reload: true)
      user_item.update_attributes({last_snapshot: snapshot})
    end

    if (params[:user_summary])
      logger.info "Updating user summary on user item #{user_item.id}"
      user_item.update_attributes({user_summary: params[:user_summary]})
    end

    user_item
  end

  def self.text_search(query)
    if query.present?
      search(query)
    else
      scoped
    end
  end

  # Returns all the items from all the users followed by the input user
  def self.from_users_followed_by(user)
    idol_ids = "SELECT idol_id FROM relationships WHERE fan_id = :user_id"
    where("user_id IN (#{idol_ids})", user_id: user.id).where(deleted: false)
  end

  private

    def self.get_snapshot(item, options)
      logger.info "options = #{options}"

      # Get today's snapshot
      snapshots = item.item_snapshots

      # There should only be one
      snapshot = snapshots.nil? ? nil : snapshots[0]
      logger.info "Number of snapshots: #{snapshots.length}"

      if snapshot.nil? || options[:force_reload]
        meta = get_meta(item.canonical_url, options)

        if snapshot.nil?
          snapshot = ItemSnapshot.create!(item: item, date:Date.today, title:
                                          meta[:title], description:
                                          meta[:description], content:
                                          meta[:content])
        else
          snapshot.update_attributes!({title: meta[:title], description:
                                       meta[:description], content:
                                       meta[:content]})
        end

        # Grab the screenshot and upload to s3 using sidekiq
        if !options[:skip_screenshot]
          ScreenshotWorker.perform_async(snapshot.id)
        end

        if options[:async_scraping]
          SnapshotWorker.perform_async(snapshot.id)
        end
      end

      snapshot
    end

    # Grab the page's meta info using MetaInspector
    # def self.get_meta(url, options)
      # if options[:skip_scraping]
        # {"url" => url, "title" => "Did not scrape", "description" => "Did not scrape"}
      # else
        # page = MetaInspector.new(url, allow_redirections: :all, timeout: 3)
        # title = page.title
        # if page.ok?
          # {"url" => page.url, "title" => title, "description" => page.description}
        # else
          # {"url" => page.url, "title" => title, "description" => nil, "error" => page.errors[0]}
        # end
      # end
    # end

    # Grab the page's meta info using Pismo
    def self.get_meta(url, options)
      if options[:skip_scraping]
        value = "Did not scrape"
        { title: value, description: value, content: value }
      elsif options[:async_scraping]
        value = "Ready soon..."
        { title: value, description: value, content: value }
      else
        # FIXME This goes into a redirection loop for urls like https://apigee.com/resources/github
        page = Pismo::Document.new(url, allow_redirections: :all)
        description = page.description.blank? ? page.lede : page.description
        { title: page.title, description: description, content: page.html_body }
      end
    end

    def self.validate_url(url)
      logger.info "Validating URL #{url}..."

      # Reject the url if we can't parse it
      begin
        uri = URI.parse(url)
      rescue URI::InvalidURIError => e
        raise Exceptions::URLInvalid.new()
      end

      logger.info "Check to see if URL is reachable..."
      # Reject the url if we can't reach it
      req = Net::HTTP.new(uri.host, uri.port)
      req.use_ssl = uri.scheme == 'https'
      count = 0
      begin
        req.request_head(uri.path.blank? ? '/' : uri.path)
      rescue SocketError => e
        raise Exceptions::URLUnresolvable.new()
      rescue Errno::ECONNRESET => e
        retry unless (count+=1) > 3
      end

      logger.info "URL #{url} appears valid"
      uri
    end

end
