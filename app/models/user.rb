class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :token_authenticatable

  before_save :ensure_authentication_token

  has_many :events, dependent: :destroy
  has_many :user_items, -> { where deleted: false }, dependent: :destroy

  has_many :idolships, foreign_key: "fan_id", class_name: "Relationship", dependent: :destroy
  has_many :fanships, foreign_key: "idol_id", class_name: "Relationship", dependent: :destroy
  has_many :idols, through: :idolships
  has_many :fans, through: :fanships

  attr_accessor :is_idol
  attr_accessor :is_fan

  include PgSearch
  pg_search_scope :search,
    against: [:email],
    using: {tsearch: {prefix: true}, trigram: {}}

  def self.text_search(query)
    if query.present?
      search(query)
    else
      scoped
    end
  end

  def following?(idol)
    idolships.find_by(idol_id: idol.id)
  end

  def follow!(idol)
    idolships.create!(idol_id: idol.id)
  end

  def unfollow!(idol)
    idolships.find_by(idol_id: idol.id).destroy!
  end

  def feed
    UserItem.from_users_followed_by(self)
  end

end
