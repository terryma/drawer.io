class ItemSnapshot < ActiveRecord::Base
  belongs_to :item
  default_scope { includes(:item).order('created_at DESC') }
  validates :item_id, presence: true
  validates :date, presence: true
  has_attached_file :screenshot,
    styles: {thumb: "300x150#"},
    storage: :s3,
    s3_credentials: "#{Rails.root}/config/s3.yml",
    bucket: "#{ENV['RAILS_ENV']}.drawer.io-screenshots",
    url: ":s3_domain_url",
    path: "/screenshots/:date/:uuid.:extension",
    default_url: lambda { |screenshot| screenshot.instance.set_default_url }

  def set_default_url
    ActionController::Base.helpers.asset_path('missing.png')
  end

  Paperclip.interpolates :uuid do |screenshot, style|
    screenshot.instance.item.uuid
  end

  Paperclip.interpolates :date do |screenshot, style|
    screenshot.instance.date.strftime('%Y/%m/%d')
  end

end
