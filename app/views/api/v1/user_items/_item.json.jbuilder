# This file is used by both the 'index' and the 'show' action to render the json
# string for an Item
json.extract! item, :id, :user_summary
if item.last_save_timestamp.nil?
  json.time_ago time_ago_in_words(item.created_at)
else
  json.time_ago time_ago_in_words(item.last_save_timestamp)
end
json.url item.last_url
json.domain item.item.domain
json.popularity item.item.popularity
if item.last_snapshot.title.nil?
  json.title 'No Title'
  json.title_ok false
else
  json.title item.last_snapshot.title
  json.title_ok true
end
if item.last_snapshot.description.nil?
  json.description 'No Description'
  json.description_ok false
else
  json.description item.last_snapshot.description
  json.description_ok true
end
json.content item.last_snapshot.content
json.has_screenshot item.last_snapshot.screenshot?
json.screenshot item.last_snapshot.screenshot.url
json.snapshot_id item.last_snapshot.id
json.owner item.user.email
json.owner_gravatar gravatar_for(item.user, size: 75)
