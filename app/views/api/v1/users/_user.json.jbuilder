json.extract! user, :email, :is_idol, :is_fan
json.gravatar gravatar_for(user, size: 75)
