json.extract! event, :client, :type
json.time_ago time_ago_in_words(event.created_at)
if event.related_user
  json.user event.related_user.email
end
if event.related_item
  json.url event.related_item.canonical_url
end
