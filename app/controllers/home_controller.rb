class HomeController < ApplicationController
  def index
    logger.info "theme = #{params[:theme]}"
    @theme = params[:theme]
    logger.info "@theme = #{@theme}"
  end
end
