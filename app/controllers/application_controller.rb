require "exceptions"

class ApplicationController < ActionController::Base
  # Turn off layout
  layout false

  # Ascending order of specificity
  rescue_from StandardError, with: :standard_error
  rescue_from Exceptions::ServerError, with: :server_error
  rescue_from Exceptions::ClientError, with: :client_error
  rescue_from Exceptions::URLAlreadyExists, with: :url_already_exists_error
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found_error

  # json requests use token_authenticatable, which isn't vulnerable against CSRF
  # attacks
  protect_from_forgery
  skip_before_action :verify_authenticity_token, if: :json_request?

  private

    def json_request?
      request.format.json?
    end

    def standard_error(error)
      logger.error "#{error.class}: #{error.message}"
      render json: { error: error.message, code: "server_error" }, :status => 500
    end

    # Server errors always return 500
    def server_error(error)
      render json: { error: error.message, code: error.code }, :status => 500
    end

    # Client errors always return 422. We might want to tweak this later. 400 is
    # also valid
    def client_error(error)
      render json: { error: error.message, code: error.code }, status: 422
    end

    def url_already_exists_error(error)
      render json: { error: error.message, code: error.code, id:
                     error.user_item.id, user_summary: error.user_item.user_summary },
                     status: 422
    end

    def record_not_found_error(error)
      render json: {
        error: "Record not found",
        code: "record_not_found"
      }
    end
end
