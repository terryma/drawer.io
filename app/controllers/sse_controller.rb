class SseController < ApplicationController

  include ActionController::Live

  def index
    response.headers['Content-Type'] = 'text/event-stream'
    ActiveRecord::Base.clear_active_connections!
    snapshot_id = params[:snapshot_id]
    @redis = get_redis()
    # Give it 30 seconds
    data = @redis.brpop("screenshot_#{snapshot_id}", {timeout: 30})
    puts "Received data from redis for key screenshot_#{snapshot_id}: #{data}"
    if data.nil?
      response.stream.write(sse(nil, {event: "screenshot_#{snapshot_id}"}))
    else
      response.stream.write(sse(JSON.parse(data[1]), {event: "screenshot_#{snapshot_id}"}))
    end
  rescue IOError
    # Client Disconnected
    puts "Stream closed"
  ensure
    @redis.quit
    response.stream.close
  end

  private
    def sse(object, options = {})
      (options.map{|k, v| "#{k}: #{v}" } << "data: #{JSON.dump object}").join("\n") + "\n\n"
    end

    # FIXME Really hacky
    def get_redis()
      if %w(staging production).include?(ENV['RAILS_ENV'])
        uri = URI.parse(ENV['REDISCLOUD_URL'])
        redis = Redis.new(host: uri.host, port: uri.port, password: uri.password)
      else
        redis = Redis.new
      end
    end
end
