class Api::V1::UsersController < Api::BaseController

  MAX_PER_PAGE = 20

  # GET /api/v1/users
  def index
    if !params[:per]
      render status: 400, json: { error: "Param 'per' is required", code: "per_page_param_required" }
      return
    end

    if params[:per].to_i > MAX_PER_PAGE
      render status: 400, json: { error: "Max per page exceeds limit of #{MAX_PER_PAGE}", code: "max_per_page_exceeds_limit" }
      return
    end

    @users = User.all.text_search(params[:q]).page(params[:page]).per(params[:per]).includes(:fans, :idols)

    @users.each do |user|
      if user.fans.include? current_user
        user.is_idol = true
      end
      if user.idols.include? current_user
        user.is_fan = true
      end
    end
  end

  # GET /api/v1/users/1
  def show
    @user = User.find(params[:id])
    # @save_events = @user.save_events
    # @user_items = @user.user_items
  end

  # GET /api/v1/profile
  def profile
    @user = current_user
    render 'show'
  end

  # POST /api/v1/follow
  def follow
    idol = User.find_by_email(params[:email])
    begin
      current_user.follow!(idol)
      current_user.events.create!(type: 'FollowEvent', client: params[:client], related_user: idol)
      idol.events.create!(type: 'FollowedEvent', client: params[:client], related_user: current_user)
    rescue ActiveRecord::RecordNotUnique
      render status: 400, json: { error: "Already followed", code: "already_followed" }
    else
      render status: 200, json: {}
    end
  end

  # POST /api/v1/unfollow
  def unfollow
    idol = User.find_by_email(params[:email])
    current_user.unfollow!(idol)
    current_user.events.create!(type: 'UnfollowEvent', client: params[:client], related_user: idol)
    idol.events.create!(type: 'UnfollowedEvent', client: params[:client], related_user: current_user)
    render status: 200, json: {}
  end

  # GET /api/v1/idols
  # TODO paginate?
  def idols
    @users = current_user.idols
    render 'index'
  end

  # GET /api/v1/feed
  def feed
    @user_items = current_user.feed.page(params[:page]).per(params[:per])
    render 'api/v1/user_items/index'
  end

  # GET /api/v1/events
  def events
    @events = current_user.events.limit(20)
    render 'api/v1/events/index'
  end
end
