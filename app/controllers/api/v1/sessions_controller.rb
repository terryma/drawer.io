class Api::V1::SessionsController < Devise::SessionsController
  respond_to :json

  def create
    # When logging in, only authenticate using username/password and not
    # auth_token
    resource = warden.authenticate!(:database_authenticatable, :scope => resource_name, :store => false, :recall => "#{controller_path}#failure")
    render :status => 200,
           :json => { :success => true,
                      :info => "Logged in",
                      :user => current_user,
                      :auth_token => current_user.authentication_token }
  end

  def destroy
    warden.authenticate!(:scope => resource_name, :store => false, :recall => "#{controller_path}#failure")
    current_user.update_column(:authentication_token, nil)
    sign_out
    render :status => 200,
           :json => { :success => true,
                      :info => "Logged out",
           }
  end

  def failure
    render status: 401,
      json: {
      success: false,
      error: "Nope that didn't work. :(",
      code: "invalid_login"
    }
  end

end

