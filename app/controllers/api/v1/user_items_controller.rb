# FIXME Ok some of these are really broken. All the actions here need to be
# taken through the current user's user items association. We can't let the
# current user update/delete stuff that he/she doesn't own
class Api::V1::UserItemsController < Api::BaseController

  def index
    @user_items = current_user.user_items.text_search(params[:q]).page(params[:page]).per(params[:per])
  end

  def create
    logger.info "Creating new user item for #{current_user.email}"
    @user_item = UserItem.new_item(current_user, item_params)
    render action: 'show', status: :created
  end

  def update
    @user_item = UserItem.update_item(current_user, item_params)
    render action: 'show', status: :ok
  end

  def destroy
    item = UserItem.find(item_params[:id])
    logger.info "Destroying item #{item.id}"
    if item.deleted?
      render json: {
        error: "Item has already been deleted",
        code: "already_deleted"
      }
    else
      item.update_attributes!({
        deleted: true,
        item_attributes: {
          popularity: item.item.popularity-1
        }
      })
      current_user.events.create({type: "DeleteEvent", client: item_params[:client], related_item: item.item})
      render json: { success: true }
    end
  end

  private

    def item_params
      params.permit(:id, :url, :client, :user_summary)
    end

end
