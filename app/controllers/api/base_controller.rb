class Api::BaseController < ApplicationController
  # before_filter :debug
  # after_filter :debug
  before_filter :authenticate_user!

  respond_to :json

  def debug()
    # logger.info request.env
    # logger.info "origin = #{request.env["HTTP_ORIGIN"]}"
    # logger.info "session = #{session.inspect}"
  end
end

