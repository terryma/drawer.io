self.port.on("load", function(url) {
  var iframeId = 'drawer-io-iframe';
  var iframe = document.getElementById(iframeId);
  if (iframe) {
    iframe.parentNode.removeChild(iframe);
  } else {
    // Only add listener once
    window.addEventListener('message', receiveMessage, false);
  }
  iframe  = document.createElement("iframe");
  iframe.src = url;
  iframe.id = iframeId;
  iframe.style.width = '100%';
  iframe.style.position = 'fixed';
  iframe.style.top = '-100px';
  iframe.style.left = '0px';
  iframe.style.height = '100px';
  iframe.style.margin = '0';
  iframe.style.padding = '0';
  iframe.style.border = '0';
  iframe.style.zIndex = '99999';
  iframe.style.MozTransition = 'all 0.5s ease-in-out';
  document.body.appendChild(iframe);

  // Needs to wait a little before the element is ready before the CSS transition
  // can take place
  window.setTimeout(function() {
    iframe.style.top = '0px';
  }, 100);

  function receiveMessage(event) {
    if (event.data.action == "close") {
      iframe = document.getElementById(iframeId);
      iframe.style.top = '-100px';
    } else if (event.data.action == "update_summary") {
      // Relay update summary requests back to main.js since we can't make a
      // cross domain ajax call here
      self.port.emit("update_summary", {id: event.data.id, summary: event.data.summary});
    }
  }

  self.port.on("done", function(data) {
    // TODO Deal with the origin security concern
    iframe.contentWindow.postMessage(data, '*');
  });

  self.port.on("close", function(data) {
    iframe.style.top = '-100px';
  });
});
