#!/bin/bash

# convert.sh main.css | pbcopy
awk -F'[()]' -v q="'" '

/background-image: url(.*)/ {
  cmd=sprintf("openssl enc -base64 -in %s | tr -d %c\\n%c",$2,q,q)
  cmd | getline b64
  close(cmd)
  $0=$1 "(data:image/png;base64," b64 ");"
}1' $1
