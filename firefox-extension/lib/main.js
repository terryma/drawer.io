var DOMAIN = "http://staging.drawer.io";
// var DOMAIN = "http://localhost:3000";
var CLIENT = "Firefox";

var widgets = require("sdk/widget");
var tabs = require("sdk/tabs");
var data = require("sdk/self").data;
var {Cc, Ci} = require("chrome");
var mediator = Cc['@mozilla.org/appshell/window-mediator;1'].getService(Ci.nsIWindowMediator);
var Request = require("sdk/request").Request
var port;

// exports.main is called when extension is installed or re-enabled
exports.main = function(options, callbacks) {
  addToolbarButton();
  // do other stuff
};

// exports.onUnload is called when Firefox starts and when the extension is disabled or uninstalled
exports.onUnload = function(reason) {
  removeToolbarButton();
  // do other stuff
};

// add our button
function addToolbarButton() {
  // this document is an XUL document
  var document = mediator.getMostRecentWindow('navigator:browser').document;
  var navBar = document.getElementById('nav-bar');
  if (!navBar) {
    return;
  }
  var btn = document.createElement('toolbarbutton');
  btn.setAttribute('id', 'drawer-toolbar-button');
  btn.setAttribute('type', 'button');
  // the toolbarbutton-1 class makes it look like a traditional button
  btn.setAttribute('class', 'toolbarbutton-1');
  // the data.url is relative to the data folder
  btn.setAttribute('image', data.url('icon_16.png'));
  btn.setAttribute('orient', 'horizontal');
  // this text will be shown when the toolbar is set to text or text and iconss
  btn.setAttribute('label', 'Drawer');
  btn.addEventListener('click', function() {
    start();
  }, false)
  navBar.appendChild(btn);
}

function removeToolbarButton() {
  // this document is an XUL document
  var document = mediator.getMostRecentWindow('navigator:browser').document;
  var navBar = document.getElementById('nav-bar');
  var btn = document.getElementById('drawer-toolbar-button');
  if (navBar && btn) {
    navBar.removeChild(btn);
  }
}

// Define keyboard shortcuts to trigger save
var { Hotkey } = require("sdk/hotkeys");

Hotkey({
  combo: "control-d",
  onPress: function() {
    start();
  }
});

// var widget = widgets.Widget({
  // id: "drawer",
  // label: "Drawer",
  // contentURL: data.url("icon_16.png"),
  // onClick: start
// });

function start() {
  port = tabs.activeTab.attach({
    contentScriptFile: data.url('iframeInjector.js')
  }).port;

  // Generate the data:url for the iframe to load. This is due to firefox not
  // supporting local iframe resources from /data in content scripts. Both
  // Chrome and Safari work fine :(
  // Since the iframe's source is a data url now, this also means all the url
  // references in the iframe will no longer work. This is why main.html is one
  // gigantic file containing all the css and javascript instead of referencing
  // them from the /data directory. This also means all the background-image and
  // font-face urls need to be converted to data:url and served locally.
  // TODO Come up with an automatic way to generate them. Someone mentioned that
  // there's a grunt task for it.
  content = encodeURIComponent(data.load("main.html"));
  url = "data:text/html;charset=utf-8,"+content;

  // Load the iframe menubar
  port.emit("load", url);

  // Handle the user updating the summary field
  port.on("update_summary", function(data) {
    updateSummary(data.id, data.summary);
  });

  // Make the ajax request to save the current link
  save(tabs.activeTab.url, CLIENT);
}

function send(key, value) {
  port.emit(key, value);
}

// Get all the cookies for our domain in a hash
function getCookies() {
  var ios = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
  var uri = ios.newURI(DOMAIN, null, null);
  var cookieSvc = Cc["@mozilla.org/cookieService;1"].getService(Ci.nsICookieService);
  var all = cookieSvc.getCookieString(uri, null);
  var cookies = {};
  if (all === null || all === "")
    return cookies;
  var list = all.split("; ");
  for(var i = 0; i < list.length; i++) {
    var cookie = list[i];
    var p = cookie.indexOf("=");
    var name = cookie.substring(0,p);
    var value = cookie.substring(p+1);
    value = decodeURIComponent(value);
    cookies[name] = value;
  }
  return cookies;
}

// Return the auth_token from the cookie
function getAuthToken() {
  return getCookies()["auth_token"];
}

// Make the ajax POST request to save the given url
function save(url, client) {
  params = {};
  params.url = url;
  params.client = client;

  auth_token = getAuthToken();

  Request({
    url: DOMAIN+'/api/v1/items',
    headers: { Authorization: 'Token token=' + auth_token },
    content: JSON.stringify(params),
    contentType: 'application/json',
    onComplete: function(response) {
      if (response.status == 201) {
        send("done", {success: true, id: response.json.id});
      } else {
        send("done", {success: false, id: response.json.id, summary: response.json.user_summary, error_code: response.json.code});
      }
    }
  }).post();
}

// Make the ajax PUT request to update the user summary for the given user item id
function updateSummary(id, summary) {
  params = {};
  params.user_summary = summary;

  auth_token = getAuthToken();

  Request({
    url: DOMAIN+'/api/v1/items/'+id,
    headers: { Authorization: 'Token token=' + auth_token },
    content: JSON.stringify(params),
    contentType: 'application/json',
    onComplete: function(response) {
      if (response.status === 200) {
        send("close", "close");
      } else {
        // TODO Handle update error
        send("close", "close");
      }
    }
  }).put();
}


