DrawerIo::Application.routes.draw do
  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      devise_for :users, :singular => :user
      devise_scope :user do
        post 'login' => 'sessions#create', :as => 'login'
        post 'logout' => 'sessions#destroy', :as => 'logout'
        get 'profile' => 'users#profile', :as => 'profile'
        post 'follow' => 'users#follow', :as => 'follow'
        post 'unfollow' => 'users#unfollow', :as => 'unfollow'
        get 'idols' => 'users#idols', :as => 'idols'
        get 'feed' => 'users#feed', :as => 'feed'
        get 'events' => 'users#events', as: 'events'
      end
      resources :users
      resources :user_items, path: "items", defaults: {format: :json}
    end
  end

  root 'home#index'

  # Handle Server Side Events
  get 'sse' => 'sse#index'

  # Handles passing the auth token to the safari extension
  get 'safari-extension' => 'safari_extension#index'

  # Handle routes that are handled by Angular
  get 'logout' => 'home#index'
  get 'profile' => 'home#index'
  get 'clients' => 'home#index'
  get 'feed' => 'home#index'
  get 'follow' => 'home#index'
end
