IMGKit.configure do |config|
  config.wkhtmltoimage = Rails.root.join('bin', 'wkhtmltoimage-amd64').to_s if %w(staging production).include?(ENV['RAILS_ENV'])
end
