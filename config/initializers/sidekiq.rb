if %w(staging production).include?(ENV['RAILS_ENV'])
  Sidekiq.configure_server do |config|
    config.redis = {url: ENV['REDISCLOUD_URL'], namespace: 'sidekiq'}
  end

  Sidekiq.configure_client do |config|
    config.redis = {url: ENV['REDISCLOUD_URL'], namespace: 'sidekiq'}
  end
end

if ENV['RAILS_ENV'] == 'development'
  Sidekiq.configure_server do |config|
    ActiveRecord::Base.logger = Logger.new(STDOUT)
  end
end
